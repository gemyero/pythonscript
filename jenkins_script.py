from jenkinsapi.jenkins import Jenkins
from datetime import datetime
import sys
import sqlite3

try:
    conn = sqlite3.connect('data.db')
    c = conn.cursor()
    c.execute('CREATE TABLE jobs (job_name text, job_status text, check_time text)')
except Exception as e:
    pass

try:
    server = Jenkins(sys.argv[1], sys.argv[2], sys.argv[3])
    for i in server.get_jobs():
        job_instance = server.get_job(i[0])
        job_name = job_instance.name
        job_status = 'enabled' if job_instance.is_enabled() else 'disabled'
        check_time = datetime.now()
        c.execute("INSERT INTO jobs VALUES ('{}', '{}', '{}')".format(job_name, job_status, check_time))
except Exception as e:
    print(e)
finally:
    conn.commit()
    conn.close()
