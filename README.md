# Prerequisites
1. Python3
2. pip3
3. jenkinsapi module

# How to run script
1. clone the repo
2. cd pythonScript
3. python3 jenkins_script.py "jenkins url" "username" "password"

for example: python3 jenkins_script.py http://localhost:8080 mohamed password